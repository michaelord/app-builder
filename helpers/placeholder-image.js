

function placeholder_url(w, h)
{
	var services			= [
		'http://satyr.io/' + w + 'x' + h,
		'http://dummyimage.com/' + w + 'x' + h,
		'https://spaceholder.cc/' + w + 'x' + h,
		'http://placecorgi.com/' + w + '/' + h,
		'http://placebear.com/' + w + '/' + h,
		'http://baconmockup.com/' + w + '/' + h,
		'http://unsplash.it/' + w + '/' + h + '?random',
		'http://placeimg.com/' + w + '/' + h + '/any',
		'http://loremflickr.com/' + w + '/' + h
	];

	var url						= services[0];

	if(false)
	{
		url						= services[Math.floor(Math.random() * services.length)];
	}

	return url;
}

exports.placeholderurl = function(w, h, options)
{
	var handlebars				= arguments[arguments.length - 1];

	if(arguments.length === 1)
	{
		options					= w;
		w						= 200;
		h						= 200;
	}

	if(arguments.length === 2)
	{
		options					= h;
		h						= w;
	}

	return placeholder_url(w, h);
}

exports.placeholder = exports.PlaceholderImage = function(w, h, options)
{
	var handlebars				= arguments[arguments.length - 1];

	if(arguments.length === 1)
	{
		options					= w;
		w						= 200;
		h						= 200;
	}

	if(arguments.length === 2)
	{
		options					= h;
		h						= w;
	}

	var string					= '';

	try
	{
		string					= new handlebars.data.root.handlebars.SafeString('<img src="' + placeholder_url(w, h) + '" width="'+w+' height="'+h+'" nopin="nopin">');
	}
	catch(e)
	{
		console.warn("ERROR (exports.placeholder)", e, arguments);

		string				= "ERROR";
	}

	return string;
}
