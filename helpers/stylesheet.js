
let fs							= require('fs');

exports.stylesheet = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();

	var str						= args[0];
	var inline					= args[1];

	var ret						= '';
	var styles					= str.split(',');

	styles.forEach(function(style)
	{
		style					= style.trim();

		if(inline)
		{
			var markup			= fs.readFileSync(style, 'utf8').toString();

			if(markup)
			{
				ret			   += markup;
			}
		}
		else
		{
			ret				   += `<link rel="stylesheet" href="/${style}">`;
		}
	});

	if(ret && inline)
	{
		ret						= `<style>${ret}</style>`;
	}

	return new options.data.root.handlebars.SafeString(String(ret));
}
