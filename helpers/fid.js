
let _id				= 0;

/**
 * [xid description]
 * @param  {Bool}   n Boolean to determine if we increment the id counter or not
 * @return {String}   A string with an id
 */
exports.fid = function()
{
	var args					= Array.prototype.slice.call(arguments);

	// options is always the last element in the array
	var options					= args.pop();

	var increase				= args[0];

	if(increase == true)
	{
		_id++
	}

	return options.data.root['guid'] + '-' + _id;
};
