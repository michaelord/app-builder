
exports.inArray = function(array, value, options)
{
	if(array.indexOf(value) !== -1)
	{
		return options.fn(this);
	}

	return options.inverse(this);

};
