
let getIcon						= require('./icon').icon;

/**
 * Placeholder to make sure that function calls don't break
 * @param  {String} icon Name of the icon to load
 * @return {String}	  SVG data for the requested icon
 */
exports.template_preview = function(preview, options)
{
	var ret						= '';

	// if there is a string supplied
	if(preview)
	{
		preview.forEach(function(value)
		{
			value				= value.trim();

			ret				   += `<img src="/assets-framework/media/components/${value}.svg">`;
		});
	}

	if(!ret)
	{
		ret						= getIcon('error', options);
	}

	return new options.data.root.handlebars.SafeString(String(ret));
};

