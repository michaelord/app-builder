
exports.stringify = function()
{
	// get the arguments as an array so we can manipulate it
	var args					= Array.prototype.slice.call(arguments);

	var options					= args.pop();

	var obj						= args[0],
		indent					= args[1] || 0;

	return JSON.stringify(obj, null, indent);
}
