
exports.csvloop = function(csv, options)
{
	var parts		= csv.split(",");
	var out			= '';

	for(var i in parts)
	{
		out	   += options.fn(parts[i]);
	}

	return out;
}
