
exports.strarr = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();

	var arr						= args[0].split(',');
	var out						= '';

	for(var i = 0; i < arr.length; i++)
	{
		arr[i]					= arr[i].trim();
		out					   += options.fn(arr[i]);
	}

	return out;
};

exports.ph = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();
	var text					= '';

	if(args[0])
	{
		text					= args[0];
	}

	return new options.data.root.handlebars.SafeString('<div class="d-placeholder">'+text+'</div>');
};

exports.avatar = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();
	var r						= Math.floor(Math.random() * 18) + 1;
	var width					= 40;

	if(args[0])
	{
		width					= args[0];
	}

	return new options.data.root.handlebars.SafeString('<img src="/placeholder/avatars/avatar-'+r+'.jpg" width="'+width+'">');
}
