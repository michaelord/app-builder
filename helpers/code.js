
exports.code = function(lang, options)
{
	try
	{
		if(arguments.length === 1)
		{
			options				= lang;
			lang				= 'html';
		}

		var code				= options.fn();
			code				= code.trim();
			code				= code.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");

		if(!code)
		{
			return '';
		}

		return '<pre><code class="language-'+lang+'">'+code+'</code></pre>';
	}
	catch(e)
	{
		console.log("Error: helpers/code.js");
	}

	return '';
}

