
let getModifiers				= require('./modifiers').modifiers;

let path						= require('path')
  , fs							= require('fs');

let data_blocks					= {};

exports.icon = function()
{
	try
	{
		let args				= Array.prototype.slice.call(arguments);
		let options				= args.pop();

		let icon				= args[0];
		let modifiers			= args[1] || '';
		let classes				= args[2] || '';

			classes				= getModifiers('icon', icon + (modifiers ? ',' : '') + modifiers, options) + (classes ? (' ' + classes) : '');

		let file				= process.cwd() + "/build/assets/media/" + icon + '.svg';

		if(!data_blocks[file])
		{
			if(fs.existsSync(file))
			{
				data_blocks[file]	= fs.readFileSync(file, 'utf8');
			}
			else
			{
				data_blocks[file]	= '<svg><use xlink:href="assets/media/icons.svg#'+icon+'"></use></svg>';
			}
		}

		let data				= data_blocks[file];
		let str					= '<i aria-hidden="true" class="'+classes+'">'+data+'</i>';

		return new options.data.root.handlebars.SafeString(String(str));
	}
	catch(e)
	{
		console.warn("Error: helpers/icon.js", e);
	}

	return '';
}
