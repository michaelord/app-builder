
module.exports = function()
{
	let args					= Array.prototype.slice.call(arguments);
	let options					= args.pop();

	return new Date().getFullYear();
};
