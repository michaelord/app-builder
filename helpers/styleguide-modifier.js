
exports.styleguide_modifier = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();

	var markup					= options.fn().trim();
	var modifier				= args[0] || false;
	var regEx					= /({modifier(\|([^}]+))?})/gi;

	markup						= markup.replace(regEx, function(all, match, def, fallback)
	{
		if(modifier)
		{
			return modifier;
		}

		if(fallback)
		{
			return fallback;
		}

		return '';
	});

	return new options.data.root.handlebars.SafeString(markup);
}
