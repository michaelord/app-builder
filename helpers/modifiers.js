
exports.modifiers = function(base, modifiers, options)
{
	if(arguments.length !== 3 || !modifiers)
	{
		return base;
	}

	if(typeof modifiers === 'string')
	{
		modifiers					= modifiers.split(',');
		modifiers					= base + '--' + (modifiers.join(' ' + base + '--'));

		return base + ' ' + modifiers;
	}

	return base;
}
