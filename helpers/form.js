
exports.formValue = function(name, fallback, options)
{
	var args					= Array.prototype.slice.call(arguments);

	// options is always the last element in the array
	var options					= args.pop();

	var value					= fallback;

	//
	if(options.data.root['formdata'])
	{
		if(options.data.root['formdata'][name] !== undefined)
		{
			value				= options.data.root['formdata'][name];
		}
	}

	return value;
};

exports.formSuccess = function()
{
	var args					= Array.prototype.slice.call(arguments);

	// options is always the last element in the array
	var options					= args.pop();

	var form					= args[0];
	var match					= args[1];

	var bool					= false;

	if(options.data.root['formdata'])
	{
		if(options.data.root['formdata']['form'] === form)
		{
			if(options.data.root['formdata']['status'] === 'pass')
			{
				bool			= true;
			}
		}
	}

	return bool === match ? options.fn(this) : options.inverse(this);
}

exports.formPosted = function()
{
	var args					= Array.prototype.slice.call(arguments);

	// options is always the last element in the array
	var options					= args.pop();

	var form					= args[0];
	var match					= args[1];

	if(match === undefined)
	{
		match					= true;
	}

	var bool					= false;

	if(options.data.root['formdata'])
	{
		if(options.data.root['formdata']['form'] === form)
		{
			bool				= true;
		}
	}

	return bool === match ? options.fn(this) : options.inverse(this);
}
