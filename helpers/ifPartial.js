
exports.ifPartial = function(name, options)
{
	return options.data.root.handlebars.partials[name] ? options.fn(this) : options.inverse(this);
};
