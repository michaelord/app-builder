
let _name				= 0;

/**
 * [xid description]
 * @param  {Bool}   n Boolean to determine if we increment the id counter or not
 * @return {String}   A string with an id
 */
exports.fname = function(n)
{
	if(typeof n === 'boolean' && n == true)
	{
		_name++
	}

	return 'name-' + _name;
};
