
exports.samplename = function(options)
{
	var choices		=
	[
		"Dan Scott",
		"Adrian Pease",
		"Michael Ord",
		"James Allis",
		"Jonny Vickers",
		"Sion Evans",
		"Gary Glozier",
		"Euan Clubbs",
		"Matt Theakston",
		"Darren Bell",
		"Chris Suter",
		"Gareth Ferguson",
		"Iain Davidson",
		"Artyom Tarasov",
		"Yuliya Petrova",
		"Alexey Rybin",
		"Nikita Markov",
		"Yuri Yasyuk",
	];

	return choices[Math.floor(Math.random() * choices.length)];
}

exports.sampleposition = function(options)
{
	var choices		=
	[
		"Engineer",
		"Project Manager",
		"Lead Infrastructure Engineer",
		"Senior Project Manager",
		"Support Manager",
		"New Business Manager",
		"Senior Software Engineer",
		"Senior Data Analyst",
		"Software Engineer"
	];

	return choices[Math.floor(Math.random() * choices.length)];
}

