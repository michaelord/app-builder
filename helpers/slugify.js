
exports.slugify = function()
{
	// get the arguments as an array so we can manipulate it
	var args					= Array.prototype.slice.call(arguments);

	var options					= args.pop();

	var string					= args[0];

	if(!string)
	{
		string					= '';
	}

	return string.toString().toLowerCase()
		.replace(/\s+/g, '-')			// Replace spaces with -
		.replace(/\/+/g, '-')			// Replace spaces with -
		.replace(/[^\w\-]+/g, '')		// Remove all non-word chars
		.replace(/\-\-+/g, '-')			// Replace multiple - with single -
		.replace(/^-+/, '')				// Trim - from start of text
		.replace(/-+$/, '');			// Trim - from end of text;
}
