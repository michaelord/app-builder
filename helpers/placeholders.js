
var content						= {};

exports.content = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();

	var name					= args[0];

	if(arguments.length === 2)
	{
		blocks[name]			= options.fn(this);
	}
	else
	{
		if(blocks[options.hash.id])
		{
			return blocks[options.hash.id];
		}

		return "";
	}
}

var blocks		= {};

exports.extend = function(/*name, inset, options*/)
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();
	var name					= args[0];
	var insert					= args[1] || 'append';

	var block					= blocks[name];

	if(!block)
	{
		block					= blocks[name]	= [];
	}

	// render the inner content
	var html					= options.fn(this);

	switch(insert)
	{
		case "before" :
		case "prepend" :
			block.unshift(html);
			break;

		case "replace" :
			block		= blocks[name]	= [];

		case "after" :
		case "append" :
		default:
			block.push(html);
			break;
	}
};

// TODO - change this to allow for contents to be passed through...
exports.block = function(/*name, options*/)
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();
	var name					= args[0];
	var insert					= args[1] || 'replace';
	var contents				= '';

	try
	{
		contents				= options.fn(this);
	}
	catch(e) {}

	//console.log(arguments)

	if(!blocks[name])
	{
		return contents;
	}

	var val						= (blocks[name] || []).join('\n');

	// clear the block
	blocks[name]				= [];

	switch(insert)
	{
		case "prepend" :
			return contents + val;

		case "replace" :
			return val;

		case "append" :
		default:
			return val + contents;
	}

	return val;
};
