'use strict';

const glob						= require('glob')
	, path						= require('path');

/**
 * Load a set of files
 * @param  {string|array} dir
 * @param  {string}       pattern
 * @return {array}
 */
module.exports = function(dir, pattern = '')
{
	let files					= []
	  , base					= null;

	dir							= !Array.isArray(dir) ? [dir] : dir;

	for (var i in dir)
	{
		base					= path.isAbsolute(dir[i]) ? '' : process.cwd();
		files					= files.concat(glob.sync(path.join(base, dir[i], pattern)));
  	}

	return files;
}
