
module.exports.standardize_path = function(path)
{
	return path.replace(/\\/g, '/');
}

module.exports.slugify = function(text)
{
	return text.toString().toLowerCase()
		.replace(/\s+/g, '-')           // Replace spaces with -
		.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
		.replace(/\-\-+/g, '-')         // Replace multiple - with single -
		.replace(/^-+/, '')             // Trim - from start of text
		.replace(/-+$/, '');            // Trim - from end of text
}

module.exports.slashit = function(str)
{
	return '/' + (str.replace(/^\//, '').replace(/\/$/, '')) + '/';
}



function normalize(path)
{
	return path.replace(/\\/g, '/').replace(/\//g, '/').replace(/\/\//g, '/').trim();
}

function merge()
{
	var obj		= {},
		i		= 0,
		il		= arguments.length,
		key;

	for (; i < il; i++)
	{
		for (key in arguments[i])
		{
			if (arguments[i].hasOwnProperty(key))
			{
				obj[key] = arguments[i][key];
			}
		}
	}

	return obj;
}

function require_uncached(module)
{
	if(require.cache[require.resolve(module)])
	{
		delete require.cache[require.resolve(module)];
	}

	return require(module);
}

function hash(str)
{
	var i, j, k, a; i = j = k = a = 0;

	if (str.length % 32 > 0) str += Array(33 - str.length % 32).join("z");
	var hash = '', bytes = [], i = j = k = a = 0, dict = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','1','2','3','4','5','6','7','8','9'];

	var ch;

	for (i = 0; i < str.length; i++ ) {
		ch = str.charCodeAt(i);
		bytes[j++] = (ch < 127) ? ch & 0xFF : 127;
	}
	var chunk_len = Math.ceil(bytes.length / 32);
	for (i=0; i<bytes.length; i++) {
		j += bytes[i];
		k++;
		if ((k == chunk_len) || (i == bytes.length-1)) {
			a = Math.floor( j / k );
			if (a < 32)
				hash += '0';
			else if (a > 126)
				hash += 'z';
			else
				hash += dict[  Math.floor( (a-32) / 2.76) ];
			j = k = 0;
		}
	}
	return hash;
}

exports.normalize				= normalize;
exports.merge					= merge;
exports.require_uncached		= require_uncached;
exports.hash					= hash;
