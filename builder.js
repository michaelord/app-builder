
let DEBUG						= false;

// -----------------------------------------------------------------------------
// Vars/Constants
// -----------------------------------------------------------------------------

const glob						= require('glob')
	, handlebars				= require('handlebars')
	, fs						= require('graceful-fs')
	, cheerio					= require('cheerio')
	, path						= require('path');

const Promise					= global.Promise || require('promise');

const loadfiles					= require('./lib/load-files');

// -----------------------------------------------------------------------------
// Helpers
// -----------------------------------------------------------------------------

function log(str)
{
	if(DEBUG)
	{
		console.log(str);
	}
}

function warn(str)
{
	if(DEBUG)
	{
		console.warn(str);
	}
}

// -----------------------------------------------------------------------------
// App
// -----------------------------------------------------------------------------

function Generator(config)
{
	log('[Generator]')

	Object.assign(this, {
		handlebars				: handlebars,
		extname					: '.html',
		//layoutsDir				: 'src/masters/**/*.html',
		layoutsDir				: null,
		//partialsDir				: 'src/partials/**/*.html',
		pagesDir				: null,
		partialsDir				: null,
		defaultLayout			: undefined,
		//helpers					: 'src/helpers/**/*.js',
		helpers					: null,
		compilerOptions			: undefined,
		debug					: false
	}, config);

	if(!this.layoutsDir)
	{
		throw new Error('Generator error: you must specify a directory for layouts');
	}

	if(!this.partialsDir)
	{
		throw new Error('Generator error: you must specify a directory for partials');
	}

	this.layouts				= {};
	this.data					= {};

	// load the built in helpers
	this.getDefaultHelpers();

	this.refresh();
}

// -----------------------------------------------------------------------------
// Layouts
// -----------------------------------------------------------------------------

Generator.prototype.updateLayout = function(filepath)
{
	log('[Generator] updateLayout()');

	let ext						= path.extname(filepath)
	  , name					= path.basename(filepath, ext)
	  , file					= fs.readFileSync(filepath);

	let layout_paths			= !Array.isArray(this.layoutsDir) ? [this.layoutsDir] : this.layoutsDir;

	for(let p = 0; p < layout_paths.length; p++)
	{
		let layout_parts		= filepath.split(layout_paths[p]);

		if(layout_parts.length > 1)
		{
			name				= layout_parts.pop();
			name				= name.replace(this.extname, '');
		}
	}

	this.layouts[name]			= this.handlebars.compile(file.toString());
}

Generator.prototype.getLayouts = function()
{
	log('[Generator] getLayouts()');

	let layouts					= loadfiles(this.layoutsDir || '!*', `**/*${this.extname}`);

	for(let l in layouts)
	{
		this.updateLayout(layouts[l]);
	}
}

Generator.prototype.getPages = function()
{
	log('[Generator] getPages()');

	let self					= this;

	return new Promise(function(resolve, reject)
	{
		let pages				= loadfiles(self.pagesDir || '!*', `**/*${this.extname}`);

		// TODO

		resolve(pages)
	});
}


// -----------------------------------------------------------------------------
// Partials
// -----------------------------------------------------------------------------

/**
 * Update the stored partial html used in handlebars
 * @param  {[type]} filepath [description]
 * @return {[type]}          [description]
 */
Generator.prototype.updatePartial = function(filepath)
{
	log('[Generator] updatePartial()');

	// get details about the file
	let ext						= path.extname(filepath)
	  , name					= path.basename(filepath, ext)
	  ,	file					= fs.readFileSync(filepath);

	// the partial html, trim the html string (just to keep thungs tidy)
	let html					= file.toString().trim();

	let partial_paths			= !Array.isArray(this.partialsDir) ? [this.partialsDir] : this.partialsDir;

	for(let p = 0; p < partial_paths.length; p++)
	{
		let partial_parts		= filepath.split(partial_paths[p]);

		if(partial_parts.length > 1)
		{
			name				= partial_parts.pop();
			name				= name.replace(this.extname, '');
		}
	}

	// console.log(name);

	if(this.debug)
	{
		html					= `<!-- partial:${name} -->\n${html}\n<!-- partial:${name} //-->`;
	}

	// register the partial so it can be used at a later time
	this.handlebars.registerPartial(name, `\n${html}\n`);
}

Generator.prototype.getPartials = function()
{
	log('[Generator] getPartials()');

	// get all the partials in the defined directories
	let partials				= loadfiles(this.partialsDir || '!*', `**/*${this.extname}`);

	// loop through all the partials, and register them
	for(let p in partials)
	{
		this.updatePartial(partials[p]);
	}
}

// -----------------------------------------------------------------------------
// Helpers
// -----------------------------------------------------------------------------

Generator.prototype.getDefaultHelpers = function()
{
	log('[Generator] getDefaultHelpers()');

	let helpers					= glob.sync(__dirname + '/helpers/**/*.js');

	for(let h in helpers)
	{
		this.loadHelper(helpers[h]);
	}
}

function requireUncached(module){
    delete require.cache[require.resolve(module)]
    return require(module)
}

Generator.prototype.loadHelper = function(filepath, debug = false)
{
	log(`[Generator] loadHelper() ${filepath}`);

	// get the name of the helper
	let name					= path.basename(filepath, '.js')
	  , helper;

	try
	{
		// if the helper is already defined
		if(this.handlebars.helpers[name])
		{
			delete require.cache[require.resolve(path.join(filepath))];

			this.handlebars.unregisterHelper(name);
		}

		// load the external helper
		helper					= requireUncached(path.join(filepath));

		if(debug)
		{
			console.log(helper);
		}

		// if the helper is a function
		if(typeof helper === 'function')
		{
			this.handlebars.registerHelper(name, helper);
		}
		else
		{
			this.handlebars.helpers	= Object.assign(this.handlebars.helpers, helper);
		}
	}
	catch(e)
	{
		warn(`Error when loading ${name}.js as a handlebars helper`);
	}
}

Generator.prototype.getHelpers = function()
{
	log('[Generator] getHelpers()');

	let helpers					= loadfiles(this.helpers || '!*');

	for(let h in helpers)
	{
		this.loadHelper(helpers[h]);
	}
	//console.log(this.handlebars.helpers)
}

// -----------------------------------------------------------------------------
// Data
// -----------------------------------------------------------------------------

Generator.prototype.getData = function()
{
	log('[Generator] getData()');
}

/**
 * Refresh all
 * @return {[type]} [description]
 */
Generator.prototype.refresh = function()
{
	log('[Generator] refresh()');

	this.getLayouts();
	this.getPartials();
	this.getHelpers();
	this.getData();
	this.getPages();
}

Generator.prototype.render = function(page, options = {})
{
	log('[Generator] render()');

	let self					= this
	  , body					= ''
	  , layout_template			= null;

	return new Promise(function(resolve, reject)
	{
		try
		{
			const defaults		= {
				layout			: null
			};

			// build the page data, initially from globals
			let page_data		= Object.assign({}, defaults, self.data);

			try
			{
				// try to get the page data
				let _page_data	= page.match(/\{\{!--([^>])*--\}\}/);

				// create a json object out of the page data
				if(_page_data)
				{
					_page_data	= _page_data[0].replace('{{!--', '').replace('--}}', '').trim();
					_page_data	= JSON.parse("{" + _page_data + "}");

					if(_page_data)
					{
						page_data	= Object.assign(page_data, _page_data);
					}
				}
			}
			catch(e)
			{
				log(e);
			}

			// add any options passed into the render function to the page data,
			// options trumps all
			page_data			= Object.assign(page_data, options);

			// by default the layout will be a default layout...
			let layout			= page_data.layout || 'default';

			// get the layout template
			layout_template		= self.layouts[layout];

			// create handlebars template out of the page
			let page_template	= self.handlebars.compile(page);

			// TODO: add more page data from various sources

			// render the page
			body				= self._renderTemplate(page_template, page_data, {});

			// if the master/layiout does not exist, implement some error checking
			if(layout_template)
			{
				body			= self._renderTemplate(layout_template, Object.assign({}, page_data, {body: body, test:"TODO"}), {});
			}
		}
		catch(e)
		{
			if(layout_template)
			{
				body			= self._renderTemplate(layout_template, Object.assign({}, {body: `template could not be parsed <br> \n <pre>${e}</pre>`}), {});
			}
			else
			{
				body			= '<!DOCTYPE html><html><head><title>Generator error</title></head><body><pre>'+e+'</pre></body></html>';
			}

			// Log the error into console
			log('Generator: rendering error ocurred.\n', e);
		}

		// TODO: allow for plugins to manipulate the content
		let $					= cheerio.load(body);

		function x(tag, content, el)
		{
			//console.log($(el).attr())

			switch(tag.toUpperCase())
			{
				case 'ROW':
					return `<div class="row">${content}</div>`;

				case 'COLUMN':
					return `<div class="column">${content}</div>`;

				case 'SPACER':
					return `<hr>`;

				case 'CONTAINER':
					return `<div class="container">${content}</div>`;

				default:
					return `<div class="unknown">${content}</div>`;
			}
		}


		function boofer()
		{
			$('container, row, column').each(function()
			{
				$(this).replaceWith(x($(this).prop("tagName"), $(this).html(), $(this)));
			});

			if($('container, row, column').length)
			{
				boofer();
			}
		}

		boofer();

		body					= $.html();



		resolve(body);
	});
}






Generator.prototype._renderTemplate = function(template, context, options)
{
	// log('[Generator] _renderTemplate()');

	return template(Object.assign({handlebars:this.handlebars}, context), options);
}




















// -----------------------------------------------------------------------------
// Gulp Plugin
// -----------------------------------------------------------------------------

const through					= require('through2'),
	  gutil						= require('gulp-util');

let generator;

module.exports					= function(options)
{
	// if there is no generator/builder create a new instance
	if(!generator)
	{
		// create a new instance of the generator
		generator						= new Generator(options);

		// update the refresh function
		module.exports.refresh			= generator.refresh.bind(generator);
		module.exports.updateHelpers	= generator.getHelpers.bind(generator);
		module.exports.updatePartial	= generator.updatePartial.bind(generator);
		module.exports.updateLayout		= generator.updateLayout.bind(generator);
	}

	// allow gulp piping
	return through.obj(function(file, enc, cb)
	{
		let self				= this;

		if(file.isNull())
		{
			cb(null, file);
			return;
		}

		if(file.isStream())
		{
			cb(new gutil.PluginError('generator', 'Streaming not supported'));
			return;
		}

		generator.render(file.contents.toString())
			.then(function(html)
			{
				// update the files contents with the newly parsed data
				file.contents			= new Buffer(html);

					// add the file back into the streal
				self.push(file);

				// This sends the modified file back into the stream
				cb();
			});
	});
}

// -----------------------------------------------------------------------------
// Expose Methods
// -----------------------------------------------------------------------------

module.exports.Generator		= Generator;
module.exports.create			= function(options)
{
	return new Generator(options);
}
module.exports.refresh			= function() {}
module.exports.updateHelpers	= function() {}
module.exports.updatePartial	= function() {}
module.exports.updateLayout		= function() {}
module.exports.help				= function()
{
	// TODO

	let txt						= `\n\nTODO\n\n\n`;

	//process.stdout.write(txt);
}

