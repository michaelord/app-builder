'use strict';

// chokidar allows us to watch more relaibily than with node
var chokidar					= require('chokidar'),
	fs							= require('fs'),
	path						= require('path'),
	utils						= require('./utils'),
	marked						= require('marked');

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ...
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var REGEX = {
		USERNAME				: /^[a-z0-9_-]{3,16}$/i,
		PASSWORD				: /^[a-z0-9_-]{6,18}$/i,
		HEX						: /^#?([a-f0-9]{6}|[a-f0-9]{3})$/i,
		SLUG					: /^[a-z0-9-]+$/i,
		EMAIL					: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
		URL						: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i,
		IP						: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
		RGB						: /rgb\(([0-9\s]+),([0-9\s]+),([0-9\s]+)/,
		FILE_EXTENSION			: /\.[^.$]+$/,
		DIRECTORY				: /[\/\\]\./,
		/**
		 * [is_hex description]
		 * @param  {[type]}  str [description]
		 * @return {Boolean}	 [description]
		 */
		is_hex : function(str)
		{
			return this.HEX.test(str);
		}
};

function uniqueArray(arr)
{
	var a = [];
    for (var i=0, l=arr.length; i<l; i++)
        if (a.indexOf(arr[i]) === -1)
            a.push(arr[i]);
    return a;
}

function remove_extension(str)
{
	return str.replace(REGEX.FILE_EXTENSION, '');
}

function normalize_imports(file_path, imports)
{
	var new_imports			= [];

	// normalize the url
	file_path				= utils.normalize(file_path);

	// loop through all the imports
	for(var i = 0; i < imports.length; i++)
	{
		// split into parts, desed on the directory seperator
		var actual_path		= file_path.split('/');

		// remove the file name from the string
		actual_path.pop();

		// normalize the import paath
		var path			= utils.normalize(imports[i]);

		// slipt the import based on the dir sep
		var path_parts		= path.split('/');

		// get the name of the file
		var file_name		= path_parts.pop();

		// trim any actual file extension
		file_name			= remove_extension(file_name);

		// sass imports by convention have a _ before the file name
		if(file_name.charAt(0) !== '_')
		{
			file_name		= '_' + file_name;
		}

		// add the extension back on
		file_name			 += '.scss';

		// add the file name back into the path parts
		path_parts.push(file_name);

		while(path_parts.length)
		{
			// remove the first item from the import path
			var dir			= path_parts.shift();

			if(dir == "" || dir == '.')
			{
				continue;
			}

			if(dir == '..')
			{
				actual_path.pop();

				continue;
			}

			actual_path.push(dir);
		}

		var import_path		= actual_path.join('/')

		new_imports.push(import_path);
	}

	return uniqueArray(new_imports);
}

function locate_block(string, start, search)
{
	var start_pos		= string.indexOf(search, start);

	// if there is no occurance of the starting string, bail out
	if(start_pos === -1)
	{
		return false;
	}

	// to match in and out of braces
	var brace_count		= 0;
	var characters		= '';

	for(var position = start_pos; position < string.length; position++)
	{
		// get the character in the mixin
		var character	= string[position];

		if(character === '{')
		{
			brace_count++;
		}

		characters		 += character;

		if(character === '}')
		{
			brace_count--;

			if(brace_count === 0)
			{
				break;
			}
		}
	}

	return {
		string		: characters,
		position	: position
	};
}

function parse_doc_comment(comment)
{
	comment					= comment.trim();

	// TODO
	var lines				= comment.split("\n");

	for(var i = 0; i < lines.length; i++)
	{
		lines[i]			= lines[i].trim();
	}

	comment					= lines.join("\n");

	// remove tabs as they're not really necessary
	comment					= comment.replace(/(\t)/g, ' ');

	// remove double spaces
	comment					= comment.replace(/(  )/g, ' ');

	// add an additional @ to the beginning of lines already starting with an @ to produce
	// @@group
	comment					= comment.replace(/\n@/g, '\n@@');

	// now split new lines starting with an @ into an array
	comment					= comment.split(/\n@/);

	var data				= {
		'title'				: '',
		'group'				: 'Unknown',
		'access'			: '',
		'alias'				: '',
		'content'			: '',
		'deprecated'		: '',
		'example'			: [],
		'link'				: [],
		'modifiers'			: [],
		'name'				: '',
		'output'			: '',
		'author'			: [],
		'param'				: [],
		'return'			: '',
		'see'				: [],
		'since'				: [],
		'todo'				: [],
		'require'			: [],
		'type'				: '',
	};

	for(var i = 0; i < comment.length; i++)
	{
		var line				= comment[i].trim();
		var parts				= line.match(/^@([a-z_-]+)(.*)/);

		// if there is no match... assume its the title
		if(!parts)
		{
			data['title']		= line;
		}
		else
		{
			try
			{
				var name			= parts[1],
					value			= parts[2];

				if(value)
				{
					value			= value.trim();
				}

				switch(name)
				{
					case 'desc'	:

						value		= line.replace('@'+name, '');
						value		= value.trim();
						value		= value.replaceAll('\n', '\n\n');

						data[name]	= marked(value);

						break;

					case 'link' :

						var o		= value.match(/((https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([^\s]+))( (.*))?/)

						if(o)
						{
							data[name].push(
								{
									'url'			: o[1],
									'caption'		: o[7]
								}
							);
						}

						break;

					case 'modifier' :

						var o		= value.split(':');

						var classes	= o.shift();
							classes	= classes.trim();

						var desc	= o.join(':');
							desc	= desc.trim();

						data['modifiers'].push(
							{
								'class'		: classes,
								'desc'		: desc
							}
						);

						break;

					case 'param' :

						var o		= value.match(/\{([^}]+)\} (\$[^\s]+)([\s]+)?(\[([^\]]+)\])?([\s]+)?(- (.*))?/);

						if(o)
						{
							data[name].push(
								{
									'type'			: o[1],
									'name'			: o[2],
									'default'		: o[5] ? o[5].trim() : '-',
									'description'	: o[8] ? o[8].trim() : '',
								}
							);
						}

						break;

					case 'see' :

						/// @see other-item
						/// @see {mixin} other-item
						/// @see $other-item
						/// @see %other-item

						// TODO

						break;

					case 'require' :

						/// @require item
						/// @require {type} item
						/// @require {type} item - description
						/// @require {type} item description
						/// @require {type} item <link>
						/// @require {type} item description <link>

						// TODO

						break;

					case 'return' :

						var o			= value.match(/\{([^}]+)\}(.*)?/);

						if(o)
						{
							data[name]	=
							{
								type		: o[1],
								description	: o[2] ? o[2].trim() : ''
							};
						}

						break;

					case 'since' :

						var o			= value.match(/([0-9.]+)(.*)?/);

						if(o)
						{
							data[name].push({
								version		: o[1],
								description	: o[2] ? o[2].trim() : ''
							});
						}

						break;

					case 'todo' :
					case 'throw' :
					case 'author' :
					case 'example' :

						data[name].push(value);
						break;

					case 'group' :

						data[name]			= value;
						data['_group']		= value.toLowerCase();

						// TODO: this better
						data['_path']		= '/styleguide/' + value.toLowerCase() + '/';

						break

					case 'access' :
					case 'alias' :
					case 'content' :
					case 'deprecated' :
					case 'name' :
					case 'output' :
					case 'type' :
					default :

						value		= doc_formatters['all'](value);

						if(doc_formatters[name])
						{
							value		= doc_formatters[name](value);
						}

						data[name]	= value;
				}
			}
			catch(e)
			{
				//console.log(line);
				//console.log(e);
				//console.log('----------------');
				//console.log(parts);
			}
		}
	}

	return data;
}

function parse_scss(file_path, sass)
{
	var props = {
		'variables'			: {},
		'functions'			: {},
		'mixins'			: {},
		'placeholders'		: {},
		'sections'			: [],
		'imports'			: [],
	};

	// split the code into lines
	var lines								= sass.split('\n');
	var in_doc_comment						= false;
	var in_s_comment						= false;

	var buffer								= '';
	var line								= 0;
	var comment_start;

	while(line < lines.length)
	{
		var string							= lines[line].trim();

		var doc_comment						= string.substring(0, 3);
		var s_comment						= string.substring(0, 5);
		var s_comment_end					= string.substring(0, 2);

		if(doc_comment === '///' && !in_s_comment)
		{
			if(!in_doc_comment)
			{
				in_doc_comment				= true;
				buffer						= '';
			}

			// add the code line to the output, minus the ///
			buffer							 += lines[line].replace('/// ', '') + '\n';
		}
		else if(in_doc_comment)
		{
			var is_placeholder				= string.charAt(0) === "%";
			var is_mixin					= string.substring(0, 6) === '@mixin';
			var is_function					= string.substring(0, 9) === '@function';

			var data						= {};

			if(is_placeholder || is_mixin || is_function)
			{
				var block					= locate_block(sass, 0, lines[line]);

				if(block)
				{
					data					= parse_doc_comment(buffer);

					// store the file that this was found in, for easy location
					data['_file']			= utils.normalize(file_path);
					data['_line']			= line + 1;

					// add the code block to the data object
					data['_code']			= block['string'].trim();

					if(is_placeholder)
					{
						var name_re			= /%([^\s\{}]+)/;
						var parts			= lines[line].match(name_re);
						data['_name']		= parts[1];
						data['_type']		= 'placeholder';
					}
					else
					{
						var FUNCTION_DATA	= /@(function|mixin) ([^(]+)([^{]+)/;
						var parts			= lines[line].match(FUNCTION_DATA);
						data['_name']		= parts[2];

						if(is_mixin)
						{
							data['_type']	= 'mixin';
						}
						else
						{
							data['_type']	= 'function';
						}
					}

					props[data['_type'] + 's'][data['_name']]	= data;
				}
			}
			// possible variable
			else
			{
				var r						= /(\$([^\s:]+))([\s]+)?:([\s]+)?([^;]+);/;
				var parts					= lines[line].match(r);

				if(parts)
				{
					data					= parse_doc_comment(buffer);

					// store the file that this was found in, for easy location
					data['_file']			= utils.normalize(file_path);
					data['_line']			= line + 1;

					data['_type']			= 'variable';
					data['_name']			= parts[1].trim();
					data['_value']			= parts[5].trim();
					data['_default']		= false;

					if(data['_value'].indexOf('!default') > -1)
					{
						// if the value has already been set... bail
						if(props['variables'][data['_name']])
						{
							line++;

							continue;
						}

						data['_default']		= true;
					}

					// strip default from the value
					data['_value']						= data['_value'].replace('!default', '');
					data['_value']						= data['_value'].trim();

					props[data['_type'] + 's'][data['_name']]	= data;
				}
			}

			in_doc_comment					= false;
		}
		else if(s_comment === '/*---')
		{
			if(!in_s_comment)
			{
				in_s_comment				= true;
				buffer						= '';
				comment_start				= line + 1;
			}
		}
		else if(in_s_comment)
		{
			if(s_comment_end == '*/')
			{
				in_s_comment				= false;

				var data					= parse_style_comment(buffer);
					data['_type']			= 'styleguide';

				// store the file that this was found in, for easy location
				data['_file']				= utils.normalize(file_path);
				data['_line']				= comment_start;

				props['sections'].push(data);
			}

			buffer							 += lines[line] + '\n';
		}

		line++;
	}

	// find all the imports
	var IMPORT_MATCH						= /(?:[^\/\/])@import ([^;]+);/g;
	var imports								= sass.match(IMPORT_MATCH);

	if(imports)
	{
		for(var i = 0; i < imports.length; i++)
		{
			var string						= imports[i];
				string						= string.replace('@import ', '');
				string						= string.replace(/(["';\s]+)/g, '');
				string						= string.split(',');

			for(var j = 0; j < string.length; j++)
			{
				props['imports'].push(string[j]);
			}
		}

		props['imports']					= normalize_imports(file_path, props['imports']);
	}

	// if everything is blank, return a blank object to save parsing and looping
	if(Object.keys(props['variables']).length === 0 && Object.keys(props['functions']).length === 0 && Object.keys(props['mixins']).length === 0 && Object.keys(props['placeholders']).length === 0 && props['sections'].length === 0 && props['imports'].length === 0)
	{
		return false;
	}

	return props;
}

var doc_formatters			= {

	all			: function(value)
	{
		if(value === 'true')
		{
			return true;
		}

		if(value === 'false')
		{
			return false;
		}

		return value;
	},

	tags		: function(value)
	{
		var tags				= value.split(',');
			tags				= tags.map(function(item, index){return item.trim().toLowerCase();});
			tags				= tags.sort();

		return tags;
	}
};

function parse_style_comment(comment)
{
	// trim down the comment
	comment					= comment.trim();

	// split the comment into 2 parts, dats and markup
	var parts				= comment.split('---');

	// document the data by callingn the default documentation function
	var data				= parse_doc_comment(parts[0]);

	// set up default markup
	data['_markup']			= '';

	//
	if(parts.length === 2)
	{
		data['_markup']		= parts[1].trim();
	}

	// return the data
	return data;
}

function build()
{
	var data					= get_data(utils.normalize(process.cwd() + SETTINGS.styles + SETTINGS.main));
	var output					= '';

	if(data)
	{
		output					= JSON.stringify(data, null, ' ');
	}

	fs.writeFile(utils.normalize(process.cwd()) + '/' + SETTINGS['output'], output);
}


function cut_hex(hex)
{
	// if the first character is a #, strip it
	if(REGEX.is_hex(hex))
	{
		return hex.substring(1);
	}

	// retutn the hex value
	return hex;
}

function hex_to_hex(hex)
{
	hex				= cut_hex(hex);

	// if this is a 3 digit hex, convert it to a 6 digiter...
	if(hex.length === 3)
	{
		hex			= hex.charAt(0) + hex.charAt(0) + hex.charAt(1) + hex.charAt(1) + hex.charAt(2) + hex.charAt(2);
	}

	// return the hex value
	return '#' + hex;
}

function hex_to_rgb(hex)
{
	// make sure that the value is a 6 figure hex value
	var hex			= hex_to_hex(hex);

	// remove the beginning #
		hex			= cut_hex(hex);

	// return the rgb value
	return {
		r			: parseInt(hex.substring(0, 2), 16),
		g			: parseInt(hex.substring(2, 4), 16),
		b			: parseInt(hex.substring(4, 6), 16)
	};
}

function rgb_to_hex(r, g, b)
{
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function get_value_tree(name, variables, arr)
{
	// if the array is not defined, set it to a blank array
	if(!arr)
	{
		arr					= [];
	}

	//  make sure that the variable is set
	if(variables[name])
	{
		// get the value of the variable
		var value				= variables[name]['_value'];

		// add the value into the array
		arr.push(value);

		// if the value is a reference to another value, call this function again
		if(value.charAt(0) === '$')
		{
			arr					= get_value_tree(value, variables, arr);
		}
	}

	// return the array tree
	return arr;
}

function clone(obj)
{
	var copy;

	// Handle the 3 simple types, and null or undefined
	if (null == obj || "object" != typeof obj)
	{
		return obj;
	}

	// Handle Date
	if (obj instanceof Date)
	{
		copy			= new Date().setTime(obj.getTime());

		return copy;
	}

	// Handle Array
	if (obj instanceof Array)
	{
		copy = [];

		for (var i = 0, len = obj.length; i < len; i++)
		{
			copy[i]				= clone(obj[i]);
		}

		return copy;
	}

	// Handle Object
	if (obj instanceof Object)
	{
		copy					= {};

		for (var attr in obj)
		{
			if (obj.hasOwnProperty(attr))
			{
				copy[attr]		= clone(obj[attr]);
			}
		}

		return copy;
	}

	throw new Error("Unable to copy obj! Its type isn't supported.");
}

function poll_imports(name)
{
	var imports				= [];

	if(SASS_DATA[name])
	{
		if(SASS_DATA[name]['imports'].length)
		{
			imports			= SASS_DATA[name]['imports'].slice(0);

			for(var i = 0; i < imports.length; i++)
			{
				imports		= imports.concat(poll_imports(imports[i]));
			}
		}
	}

	return imports;
}

function get_data(start)
{
	if(!SASS_DATA[start])
	{
		return false;
	}

	var st				= new Date().getTime();

	// get a list of all the imports that should be used in the demo
	var imports			= poll_imports(start);

		// make sure we don't document things twice
		imports			= uniqueArray(imports);

		// filter out any libraries, as we probably don't want to document them
		// or they won't be formatted to the same standard as our internal docs
		imports			= imports.filter(function(i)
		{
			return i.indexOf('lib-') == -1;
		});

	var demo			= clone(SASS_DATA[start]);

	delete demo['imports'];

	for(var i = 0; i < imports.length; i++)
	{
		var data		= SASS_DATA[imports[i]];

		if(data)
		{
			for(var j in data['variables'])
			{
				if(demo['variables'][j] && data['variables'][j]['_default'])
				{
					continue;
				}

				demo['variables'][j]		= data['variables'][j];
			}

			// loop through all the variables
			for(var j in demo['variables'])
			{
				// get the tree of values for the object - we do this because the
				// value may be a reference to another variable
				var value_tree		= get_value_tree(j, demo['variables'], []);

				// get the end value of the colour
				var true_value		= value_tree[value_tree.length - 1];

				// if the variable is a colour, we need to do some colour selecting
				if(demo['variables'][j]['type'] === 'Color')
				{
					// TODO /^#?([a-f0-9]{6}|[a-f0-9]{3})$/

					// if the colour is a hex value...
					if(REGEX.is_hex(true_value))
					{
						var hex		= true_value;
						var rgb		= hex_to_rgb(hex);

						demo['variables'][j]['hex']		= hex;
						demo['variables'][j]['rgb']		= rgb;
					}
					else if(true_value.substring(0, 3) == 'rgb')
					{
						// trim spaces
						var rgb							= true_value.replace(/\s/g, '');
							rgb							= rgb.match(REGEX.RGB);

						if(rgb)
						{
							rgb		= {
								r	: Number(rgb[1]),
								g	: Number(rgb[2]),
								b	: Number(rgb[3])
							};

							var hex	= rgb_to_hex(rgb.r, rgb.g, rgb.b);

							demo['variables'][j]['hex']	= hex;
							demo['variables'][j]['rgb']	= rgb;
						}
					}
				}
				else
				{
				}
			}

			for(var j in data['functions'])
			{
				demo['functions'][j]		= data['functions'][j];
			}

			for(var j in data['mixins'])
			{
				demo['mixins'][j]		= data['mixins'][j];
			}

			for(var j in data['placeholders'])
			{
				demo['placeholders'][j]	= data['placeholders'][j];
			}

			if(data['sections'].length)
			{
				demo['sections']	= demo['sections'].concat(data['sections']);
			}
		}
	}

	var _navigation			= {},
		navigation			= [],
		_sections			= {};




	var application_tags		= {};


	for(var i = 0; i < demo['sections'].length; i++)
	{
		var section				= demo['sections'][i];




		// ---------------------------------------------------------------------

		// if there are tags
		if(section.tags)
		{
			// loop through all the tags on the selected item
			for(var _t = 0; _t < section.tags.length; _t++)
			{
				// if the tag hasn't been defined yet
				if(application_tags[section.tags[_t]] == undefined)
				{
					// create the tag mapping
					application_tags[section.tags[_t]]		= [];
				}

				// add the reference to the application tags
				application_tags[section.tags[_t]].push(i);
			}
		}

		// ---------------------------------------------------------------------











		// get the title of the styleguide item
		var title				= section['title'];

		// the grouping
		var group				= section['group'];

		// generate a clean urr
		var url					= group.toLowerCase();
		var hash				= title.toLowerCase();

		section['_id']			= hash;

		if(!_navigation[group])
		{
			_navigation[group]	= true;

			// split the group up by the name (as it may contain a '/')
			var group_parts		= group.split('/');

			if(group_parts.length > 1)
			{
				var group_part	= group_parts[0];

				if(!_sections[group_part])
				{
					_sections[group_part]	= [];
				}

				_sections[group_part].push
				(
					{
						"name"		: group,
						"url"		: '/styleguide/' + url + '/'
					}
				)

				if(!_navigation[group_part])
				{
					_navigation[group_part]	= true;

					navigation.push
					(
						{
							"name"		: group_part,
							"url"		: '/styleguide/' + group_part.toLowerCase() + '/',
							"children"	: false
						}
					)
				}
			}
			else
			{
				navigation.push
				(
					{
						"name"		: group,
						"url"		: '/styleguide/' + url + '/',
						"children"	: false
					}
				)
			}
		}
	}

	var compare					= function(a, b)
	{
		if (a.name < b.name)
		{
			return -1;
		}

		if (a.name > b.name)
		{
			return 1;
		}

		return 0;
	};

	navigation.sort(compare);

	for(var i = 0; i < navigation.length; i++)
	{
		if(_sections[navigation[i].name])
		{
			navigation[i].children	= _sections[navigation[i].name].sort(compare);

			for(var j = 0; j < navigation[i].children.length; j++)
			{
				var n			= navigation[i].children[j].name;
				var p			= n.split('/');
					p.shift();

				navigation[i].children[j].display		= p.join(': ');
			}
		}
	}

	demo['sections-nav']		= navigation;

	demo['tags']				= application_tags;

	return demo;
}

var SASS_DATA					= {};
var SASS_MAIN					= {};
var SETTINGS					= {};

/**
 * [StyleguideEngine description]
 * @param {[type]} options [description]
 */
function StyleguideEngine(options)
{
	var DEFAULTS = {
		styles					: '/src/packages/',
		persistent				: true,
		main					: 'application.scss',
		output					: 'application.json',
	};

	SETTINGS					= utils.merge({}, DEFAULTS, options);

	var _delay_timer			= null;

	var __styles_contents		= function(path)
	{
		var contents			= fs.readFileSync(path, 'utf8');
		var props				= parse_scss(path, contents);

		SASS_DATA[utils.normalize(path)]	= props;

		clearTimeout(_delay_timer);

		_delay_timer			= setTimeout(build, 100);
	}

	var __styles				= chokidar.watch
	(
		[
			utils.normalize(process.cwd() + SETTINGS.styles) + '*.scss',
			utils.normalize(process.cwd() + SETTINGS.styles) + '**/*.scss'
		],
	{
		ignored					: /[\/\\]\./,
		persistent				: SETTINGS.persistent
	});

	__styles.on('add', function(path, stats)
	{
		var file				= utils.normalize(path);
		var parts				= file.split('/');
		var name				= parts.pop();

		if(name.charAt(0) != '_')
		{
			SASS_MAIN[file]		= true;
		}

		__styles_contents(path);
	});

	__styles.on('change', __styles_contents);

	__styles.on('unlink', function(path, stats)
	{
		console.log("remove data for " + path);

		var file				= utils.normalize(path);
		var parts				= file.split('/');
		var name				= parts.pop();

		if(name.charAt(0) != '_')
		{
			if(SASS_MAIN[file])
			{
				delete SASS_MAIN[file];
			}
		}

		if(SASS_DATA[path])
		{
			delete SASS_DATA[path];
		}
	});

	//console.log("StyleguideEngine()");
}

StyleguideEngine.example = function()
{
	//console.log("StyleguideEngine.example()");
}

module.exports					= StyleguideEngine;
