
let config						= {
	task_prefix					: ''
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, sass						= require('gulp-sass')
	, postcss					= require('gulp-postcss')
	, cssvariables				= require('postcss-css-variables')
	, autoprefixer				= require('autoprefixer')
	, flexbox_fixes				= require('postcss-flexbugs-fixes')
	, flexibility				= require('postcss-flexibility')
	, rename					= require('gulp-rename')
	, mqpacker					= require("css-mqpacker")
	, csscomb					= require('gulp-csscomb')
	, sourcemaps				= require('gulp-sourcemaps')
	, cssnano					= require('cssnano')
	, notify					= require('gulp-notify')
	, gulpif					= require('gulp-if')
	, plumber					= require('gulp-plumber');

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// main build task
	gulp.task(`${config.task_prefix}styles`, function()
	{
		// plumber error handler to stop things from breaking on errors
		var plumberErrorHandler	= {
			errorHandler : notify.onError
			(
				{
					title		: "Gulp",
					message		: "Error: <%= error.message %>"
				}
			)
		};

		var process_autoprefix	= autoprefixer
		(
			{
				browsers 		: config.autoprefix_browsers,
				cascade 		: false
			}
		);

		var process_nano		= cssnano
		(
			{
				autoprefixer	: false
			}
		);

		var processors			= [flexbox_fixes];
			processors.push(mqpacker);

			// Add autoprefix post CSS
			processors.push(process_autoprefix);

			processors.push(cssvariables());

			// processors.push(flexibility);

		return gulp.src(config.build_paths)
			// Plumber to stop errors killing the process
			.pipe(plumber(plumberErrorHandler))
			//
			.pipe(sourcemaps.init())
			// SASS this bad boy
			.pipe(sass
				(
					{
						outputStyle		: "expanded",
						errLogToConsole	: true,
						indentType 		: 'tab',
						indentWidth		: 1,
					}
				)
			)
			// Format
			.pipe(csscomb())
			// run the core CSS PostProcesses
			.pipe(postcss(processors))
			// Save off the uncompressed CSS
			//.pipe(gulpif(config.production, gulp.dest(config.dest)))
			// Minify the CSS
			.pipe(gulpif(config.production, postcss([process_nano])))
			// Rename the output file to a minified extension
			.pipe(rename({extname:'.min.css'}))
			//
			//.pipe(sourcemaps.write('.'))
			// Save off the minified file
			.pipe(gulp.dest(config.dest));
	});

	// watch files
	gulp.task(`${config.task_prefix}styles:watch`, function()
	{
		watch(config.paths, function(vinyl)
		{
			gulp.start.apply(gulp, [`${config.task_prefix}styles`]);
		});
	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}styles            - compile the styles\n` +
				  `${config.task_prefix}styles:watch      - watch and build the styles on file changes`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
