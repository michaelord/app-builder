
let config						= {
	pagesDir					: null,
	layoutsDir					: null,
	partialsDir					: null,
	task_prefix					: ''
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, builder					= require('app-builder/builder');

// -----------------------------------------------------------------------------
// Methods
// -----------------------------------------------------------------------------

/**
 * Method used to compile the templates
 * We do this because gulp doesn not allow arguments to be passed into a
 * @param  {Array}  src The path/glob to use to compile the files
 * @return {Object}     Gulp runner
 */
function _templates(src)
{
	return gulp
		.src(src)
		.pipe(builder(config))
		.pipe(gulp.dest('build'));
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports.setup = function(options)
{
	let page_individual			= '';

	config						= Object.assign({}, config, options);

	let pages					= !Array.isArray(config.pagesDir) ? [config.pagesDir] : config.pagesDir;
		pages					= pages.map(function(x){ return `${x}**/*.html` });

	let layouts					= !Array.isArray(config.layoutsDir) ? [config.layoutsDir] : config.layoutsDir;
		layouts					= layouts.map(function(x){ return `${x}**/*.html` });

	let partials				= !Array.isArray(config.partialsDir) ? [config.partialsDir] : config.partialsDir;
		partials				= partials.map(function(x){ return `${x}**/*.html` });

	let helpers					= !Array.isArray(config.helpers) ? [config.helpers] : config.helpers;

	gulp.task(`${config.task_prefix}templates`, function()
	{
		return _templates(pages);
	});

	gulp.task(`${config.task_prefix}template`, function()
	{
		return _templates(page_individual);
	});

	gulp.task(`${config.task_prefix}templates:watch`, function()
	{
		// watch pages for changes
		watch(pages, function(vinyl)
		{
			page_individual			= vinyl.path;

			gulp.start.apply(gulp, [`${config.task_prefix}template`]);
		});

		// watch masters for changes
		watch(layouts, function(vinyl)
		{
			// update the changed partial
			builder.updateLayout(vinyl.path);

			// build the templates
			gulp.start.apply(gulp, [`${config.task_prefix}templates`]);
		});

		// watch partials for changes
		watch(partials, function(vinyl)
		{
			// update the changed partial
			builder.updatePartial(vinyl.path);

			// build the templates
			gulp.start.apply(gulp, [`${config.task_prefix}templates`]);
		});

		// TODO: update helpers
		watch(helpers, function(vinyl)
		{
			builder.updateHelpers();
		});
		// TODO: watch data
	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}templates         - compile the templates\n` +
				  `${config.task_prefix}templates:watch   - watch and build the templates on file changes`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
