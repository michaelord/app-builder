
let config						= {
	task_prefix					: ''
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, sourcemaps				= require('gulp-sourcemaps')
	, source					= require('vinyl-source-stream')
	, buffer					= require('vinyl-buffer')
	, browserify				= require('browserify')
	, watchify					= require('watchify')
	, babel						= require('babelify')
	, esdoc						= require("gulp-esdoc")
	, notify					= require('gulp-notify')
	, plumber					= require('gulp-plumber')
	, gulpif					= require('gulp-if')
	, rename					= require('gulp-rename')
	, del						= require('del')
	, uglify					= require('gulp-uglify');

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// main build task
	gulp.task(`${config.task_prefix}scripts`, function()
	{
		var bundler	= browserify(config.src,
			{
				debug			: false
			}
		).transform
		(
			babel.configure
			(
				{
					// Use all of the ES2015 spec
					presets		: ["es2015"]
				}
			)
		);

		var plumberErrorHandler	= {
			errorHandler		:notify.onError(
				{
					title		:"Gulp",
					message		:"Error: <%= error.message %>"
				}
			)
		};

		return bundler.bundle()
			.pipe(plumber(plumberErrorHandler))
			//
			//.pipe(sourcemaps.init())
			//
			.pipe(source(config.src_file))
			// Save off the uncompressed JS
			.pipe(gulpif(config.production, gulp.dest(config.dest)))
			// Rename the output file to a minified extension
			.pipe(rename({extname:'.min.js'}))
			// Compress the JS
			.pipe(gulpif(config.production, uglify(
				{
					// http://lisperator.net/uglifyjs/compress
					compress :
					{
						global_defs :
						{
							DEBUG		: false
						}
					}
				}
			)))
			//
			//.pipe(sourcemaps.write('.'))
			// Save off the minified file
			.pipe(gulp.dest(config.dest));
	});

	// watch files
	gulp.task(`${config.task_prefix}scripts:watch`, function()
	{
		watch(config.paths, function(vinyl)
		{
			gulp.start.apply(gulp, [`${config.task_prefix}scripts`]);
		});
	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}scripts           - compile the scripts\n` +
				  `${config.task_prefix}scripts:watch     - watch and build the scripts on file changes`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
