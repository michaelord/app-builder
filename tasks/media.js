
let config						= {
	task_prefix					: ''
};

const gulp						= require('gulp'),
	  watch						= require('gulp-watch');

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// main build task
	gulp.task(`${config.task_prefix}media`, function()
	{

	});

	// watch files
	gulp.task(`${config.task_prefix}media:watch`, function()
	{

	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}media             - compile the scripts\n` +
				  `${config.task_prefix}media:watch       - watch and build the scripts on file changes`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
