
let config						= {
	task_prefix					: '',
	coloured					: false
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, cheerio					= require('gulp-cheerio')
	, svgstore					= require('gulp-svgstore')
	, svgmin					= require('gulp-svgmin')
	, path						= require('path');

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// main build task
	gulp.task(`${config.task_prefix}icons`, function()
	{
		return gulp
			.src(config.src)
			.pipe(svgmin(function (file)
			{
	            var prefix		= path.basename(file.relative, path.extname(file.relative));

	            return {
	                plugins		:
	                [
		                {
		                    cleanupIDs	:
		                    {
		                        prefix	: prefix + '-',
		                        minify	: true
		                    }
		                }
					]
	            }
	        }))
			.pipe(cheerio(
			{
				run: function ($)
				{
					if(!config.coloured)
					{
						// Remove fill attribute to allow total CSS control
						$('[fill]').removeAttr('fill');
						$('[id]').removeAttr('id');
					}
				},
				parserOptions	:
				{
					xmlMode		: true
				}
			}))
			.pipe(gulp.dest(config.dest))
			.pipe(svgstore(

			))
			.pipe(gulp.dest(config.dest));
	});

	// watch files
	gulp.task(`${config.task_prefix}icons:watch`, function()
	{
		watch(config.src, function(vinyl)
		{
			gulp.start.apply(gulp, [`${config.task_prefix}icons`]);
		});
	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}icons             - compile the icons\n` +
				  `${config.task_prefix}icons:watch       - watch and build the icons on file changes`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
