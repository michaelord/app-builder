
let config						= {
	pagesDir					: null,
	layoutsDir					: null,
	partialsDir					: null,
	task_prefix					: ''
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, builder					= require('app-builder/builder')
	, utils						= require('app-builder/lib/utils')
	, fs						= require('fs');

	//const styleguide			= require('app-builder/styleguide');
const styleguide				= require('app-builder/new-stylesheet');

// -----------------------------------------------------------------------------
// Methods
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------
module.exports.setup = function(options)
{
	options						= Object.assign({}, {
		base					: "src/packages/",
		main					: "application",
		output					: "/data/application.json",
		styles					: "",
		persistant				: true
	}, options);

	gulp.task(`${config.task_prefix}styleguide`, function()
	{
		styleguide(
			{
				base			: options.base,
				main			: options.main,
				formatters		:
				{
					download	: function(value)
					{
						return value;
					}
				}
				//main			: 'apptest'
			}
		)
		.then(function(data)
		{
			let navigation		= {};

			for(let d in data.guides)
			{
				let obj			= data.guides[d];

				// if there is no slud defined, fallback
				if(!obj.slug)
				{
					obj.slug	= "style-guide";
				}

				// make sure that the slug is standardised
				obj.slug		= utils.slashit(obj.slug.toLowerCase());

				// trim down the slug parts
				let slug		= obj.slug.replace(/^\//, '').replace(/\/$/, '');
				let parts		= slug.split('/');

				let ref			= navigation;
				let path		= '';

				for(var i = 0; i < parts.length; i++)
				{
					path		= utils.slashit(path + parts[i]);

					if(!ref[parts[i]])
					{
						ref[parts[i]]	= {path:path, sections:[]}
					}

					ref			= ref[parts[i]];
				}

				ref.sections.push({title: obj.title, id : utils.slugify(obj.title)});
			}

			data.navigation		= navigation;

			fs.writeFile(process.cwd() + options.output, JSON.stringify(data, null, ' '));
		});
	});

	if(options.persistant)
	{
		watch(options.styles, function(vinyl)
		{
			styleguide.refreshfile(vinyl.path);

			gulp.start.apply(gulp, [`${config.task_prefix}styleguide`]);
		});
	}

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}styleguide        - compile the styleguide`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
