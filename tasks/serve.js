
let config						= {
	task_prefix					: '',
	port						: 2345
};

const gulp						= require('gulp')
	, watch						= require('gulp-watch')
	, browserSync				= require('browser-sync');

const bs						= browserSync.create('templates');

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// main build task
	gulp.task(`${config.task_prefix}serve`, function()
	{
		bs.init(
		{
			port				: config.port,
			notify				: false,
			reloadDebounce		: 200,
			logPrefix			: "Templates",
			server:
			{
				baseDir			: config.base,
				directory		: false
			}
		});

		files			   = [
			`${config.base}/**`
		];

		watch(files, {}, function()
		{
			bs.reload();
		});
	});

	module.exports.info = function()
	{
		let txt = `${config.task_prefix}serve             - compile the scripts`;

		console.log(txt);
	}
}

// placeholder this will get overritten once the setup function is called
module.exports.info = function() {}
