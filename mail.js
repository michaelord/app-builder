
const through					= require('through2'),
	  gutil						= require('gulp-util');

const nodemailer				= require('nodemailer')
	, cheerio					= require('cheerio')
	, inlineBase64				= require('nodemailer-plugin-inline-base64');

module.exports					= function(options)
{
	// allow gulp piping
	return through.obj(function(file, enc, cb)
	{
		let self				= this;

		if(file.isNull())
		{
			cb(null, file);
			return;
		}

		if(file.isStream())
		{
			cb(new gutil.PluginError('mail', 'Streaming not supported'));
			return;
		}

		let html				= file.contents.toString();

		//console.log(html);

		// load the html through cheerio
		let $					= cheerio.load(html);

		options = Object.assign({},
			{
				from			: null,
				to				: null,
				user			:
				{
					email		: null,
					pass		: null,
				},
				subject			: $('head title').text(),
				text			: '',
				html			: html
			}, options);

		if(!options.user.email || !options.user.pass)
		{
			cb(new gutil.PluginError('mail', 'Gmail credentials required to send mail'));
			return;
		}

		if(!options.from)
		{
			cb(new gutil.PluginError('mail', 'From Email Address is required'));
			return;
		}

		if(!options.to)
		{
			cb(new gutil.PluginError('mail', 'To Email Address is required'));
			return;
		}

		let transporter			= nodemailer.createTransport
		(
			{
				service			: 'gmail',
				auth:
				{
					user		: options.user.email,
					pass		: options.user.pass
				}
			}
		);

		transporter.use('compile', inlineBase64({cidPrefix: 'somePrefix_'}));

		transporter.sendMail(options, function(err, info)
		{
			if (err)
			{
				cb(new gutil.PluginError('mail', 'Failed to send mail: ' + err));
				return;
			}

			self.push(file);

			// This sends the modified file back into the stream
			cb();

			//console.log('Message %s sent: %s', info.messageId, info.response);
		});
	});
}
