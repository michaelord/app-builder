'use strict';

const fs						= require('fs')
	, path						= require('path')
	, glob						= require('glob')
	, marked					= require('marked')

const loadfiles					= require('./lib/load-files')
	, utils						= require('./lib/utils');

const REGEX_IMPORTS_ALL			= /(?:[^\/\/])@import ([^;\(]+);/g

/**
 * [Parser description]
 * @param {[type]} options [description]
 */
function Parser(options)
{
	// default settings
	let defaults				=
	{
		base					: '',
		main					: '',
		formatters				: {}
	};

	// set the values
	Object.assign(this, defaults, options);

	//
	this._base					= utils.standardize_path(path.normalize(process.cwd() + '/' + this.base));

	// store for all the file date
	this.filedata				= {};

	// load all the files
	this.refresh();
}

/**
 * [parse description]
 * @return {[type]} [description]
 */
Parser.prototype.parse = function()
{
	let self					= this;

	return new Promise(function(resolve, reject)
	{
		resolve(self.filedata);
	})
}

/**
 * [refreshfile description]
 * @param  {[type]} filepath [description]
 * @return {[type]}          [description]
 */
Parser.prototype.refreshfile = function(filepath)
{
	let self					= this;

	// make sure that the path to the file is standardised
	filepath					= utils.standardize_path(filepath);

	let relative_path_ext		= filepath.replace(this._base, '')

	// get the relative path from the base, and remove the file extension as scss doesn't require it
	let relative_path			= relative_path_ext.replace('.scss', '');

	// get the basepath of the file
	let base_path				= path.dirname(filepath);

	// load the contents of the file
	let contents				= fs.readFileSync(filepath, 'utf8');

	// if there is no contents.. exit immediately
	if(!contents.trim())
	{
		return {};
	}

	// properties in the file
	let scssdata				=
	{
		'variables'				: {},
		'functions'				: {},
		'mixins'				: {},
		'placeholders'			: {},
		'imports'				: [],
	};

	// split the contents up into individual lines
	let lines					= contents.split('\n');

	// variables used in parsing
	let in_doc_comment			= false
	  , in_s_comment			= false
	  , buffer					= ''
	  , line					= 0
	  , comment_start			= null
	  , string					= null;

	// loop through everything, line by line
	while(line < lines.length)
	{
		// trim down the line so we can work out what we're dealing with
		string					= lines[line].trim();

		//
		if(string.substring(0, 3) === '///' && !in_s_comment)
		{
			if(!in_doc_comment)
			{
				in_doc_comment	= true;
				buffer			= '';
			}

			// add the code line to the output, minus the ///
			buffer			   += lines[line].replace('/// ', '') + '\n';
		}
		// if in a comment (///)
		else if(in_doc_comment)
		{
			let is_placeholder	= string.charAt(0) === "%"
			  , is_mixin		= string.substring(0, 6) === '@mixin'
			  , is_function		= string.substring(0, 9) === '@function';

			let data			= {};

			if(is_placeholder || is_mixin || is_function)
			{

		  		let block	= locate_block(contents, 0, lines[line]);

		  		if(block)
		  		{
		  			data					= Object.assign(self.parse_doc_comment(buffer),
		  				{
		  					_file			: relative_path_ext,
		  					_line			: line + 1,
		  					_code			: block['string'].trim()
		  				}
		  			);

					if(is_placeholder)
					{
						var name_re			= /%([^\s\{}]+)/;
						var parts			= lines[line].match(name_re);
						data['_name']		= parts[1];
						data['_type']		= 'placeholder';
					}
					else
					{
						var FUNCTION_DATA	= /@(function|mixin) ([^(]+)([^{]+)/;
						var parts			= lines[line].match(FUNCTION_DATA);
						data['_name']		= parts[2];
						data['_type']		= parts[1];
					}

					scssdata[data['_type'] + 's'][data['_name']]	= data;

					/*
					if(data['section'])
					{
						let section			= data['section'];

						if(!scssdata[`${section}s`])
						{
							scssdata[`${section}s`]	= [];
						}

						scssdata[`${section}s`].push(data);
					}
					*/
			  	}
			}
			// possible variable
			else
			{
				var r						= /(\$([^\s:]+))([\s]+)?:([\s]+)?([^;]+);/;
				var parts					= lines[line].match(r);

				if(parts)
				{
					data					= Object.assign(self.parse_doc_comment(buffer), {
						_file				: relative_path_ext,
						_line				: line + 1,
						_type				: 'variable',
						_name				: parts[1].trim(),
						_value				: parts[5].trim(),
						_default			: false
					})

					if(data['_value'].indexOf('!default') > -1)
					{
						// if the value has already been set... bail
						if(scssdata['variables'][data['_name']])
						{
							line++;

							continue;
						}

						data['_default']		= true;
					}

					// strip default from the value
					data['_value']						= data['_value'].replace('!default', '');
					data['_value']						= data['_value'].trim();

					scssdata[data['_type'] + 's'][data['_name']]	= data;

					if(data['slug'])
					{
						let section			= 'section';

						if(!scssdata[`${section}s`])
						{
							scssdata[`${section}s`]	= [];
						}

						scssdata[`${section}s`].push(data);
					}
				}
			}
			in_doc_comment					= false;
		}
		else if(string.substring(0, 5) === '/*---')
		{
			if(!in_s_comment)
			{
				in_s_comment				= true;
				buffer						= '';
				comment_start				= line + 1;
			}
		}
		else if(in_s_comment)
		{
			if(string.substring(0, 2) == '*/')
			{
				in_s_comment				= false;


				// determine the grouping, by default
				let section					= 'section';

				var data					= self.parse_style_comment(buffer);
					data['_type']			= 'styleguide';

				// if a section has been defined, set the new one
				if(data['section'])
				{
					section		= data['section'];
				}

				// store the file that this was found in, for easy location
				data['_file']				= relative_path_ext;
				data['_line']				= comment_start;

				// if the grouphasn't been set
				if(!scssdata[`${section}s`])
				{
					scssdata[`${section}s`]	= [];
				}

				scssdata[`${section}s`].push(data);
			}

			buffer							 += lines[line] + '\n';
		}

		// move onto the next line
		line++
	}

	// match all the imports in the file
	let imports					= contents.match(REGEX_IMPORTS_ALL);

	if(imports)
	{
		// loop through all the imports
		for(let i = 0; i < imports.length; i++)
		{
			// trim down
			let include			= imports[i].replace('@import ', '').replace(/(["';\s]+)/g, '').split(',');

			// loop through all the imports
			for(let j = 0; j < include.length; j++)
			{
				// make sure that the file has the .scss extension
				let file		= include[j].replace('.scss', '') + '.scss';

				// get the full path to the file
				file			= path.resolve(base_path, file);

				// get the path to the file from the base, this wil lallow for a quick lookup of the import
				file			=  utils.standardize_path(file).replace(this._base, '').replace('.scss', '')


				let parts		= path.parse(file);

				if(parts.base.charAt(0) !== '_')
				{
					parts.base	= `_${parts.base}`;
				}

				file			= utils.standardize_path(path.format(parts));

				scssdata.imports.push(file);
			}
		}
	}

	// make sure that there aren't any duplicated imports
	scssdata.imports			= scssdata.imports.filter((x, i, a) => a.indexOf(x) == i);

	// clean up so the file isn't filled with items that are blank
	if(Object.keys(scssdata['variables']).length    === 0) { delete scssdata['variables']; }
	if(Object.keys(scssdata['functions']).length    === 0) { delete scssdata['functions']; }
	if(Object.keys(scssdata['mixins']).length       === 0) { delete scssdata['mixins']; }
	if(Object.keys(scssdata['placeholders']).length === 0) { delete scssdata['placeholders']; }
	if(scssdata['imports'].length                   === 0) { delete scssdata['imports']; }

	// store the file information
	this.filedata[relative_path]	= scssdata;
}

/**
 * [refresh description]
 * @return {[type]} [description]
 */
Parser.prototype.refresh = function()
{
	// get a list of all the scss files
	let files					= loadfiles(this.base || '!*', '**/*.scss');

	// loop through all the scss files
	for(let f in files)
	{
		// read the file in
		this.refreshfile(files[f]);
	}
}

/**
 * [compile description]
 * @return {[type]} [description]
 */
Parser.prototype.compile = function()
{
	let self					= this;

	// function to recursively get all the imports
	let get_imports = function(name)
	{
		let imports				= [];

		if(self.filedata[name])
		{
			if(self.filedata[name]['imports'])
			{
				// TODO: get these in a better fashion
				imports			= self.filedata[name]['imports'].slice(0);

				for(let i = 0; i < self.filedata[name]['imports'].length; i++)
				{
					imports		= imports.concat(get_imports(imports[i]));
				}
			}
		}

		return imports;
	}

	return new Promise(function(resolve, reject)
	{
		// load and parse all the files
		self.parse()
			.then(function(data)
			{
				// get the imports
				let imports		= get_imports(self.main);

				// make sure that things don'e get documented twice
				imports			= imports.filter((x, i, a) => a.indexOf(x) == i);

				//
				let result		= Object.assign({}, self.filedata[self.main]);

				// loop through all the imports
				for(let i in imports)
				{
					// reference to the imported data
					let data	= self.filedata[imports[i]];

					for(let key in data)
					{
						switch(key)
						{
							case 'variables':

			if(!result.variables)
			{
				result.variables	= {};
			}

			for(var j in data['variables'])
			{
				if(result['variables'][j] && data['variables'][j]['_default'])
				{
					continue;
				}

				result['variables'][j]		= data['variables'][j];
			}

			try
			{


				for(var j in data['variables'])
				{
					// get the tree of values for the object - we do this because the
					// value may be a reference to another variable
					var value_tree		= get_value_tree(j, result['variables'], []);

					// get the end value of the colour
					var true_value		= value_tree[value_tree.length - 1];

					if(true_value)
					{
						// if the variable is a colour, we need to do some colour selecting
						if(data['variables'][j]['type'])
						{
							if(data['variables'][j]['type'] === 'Color')
							{
								// if the colour is a hex value...
								if(REGEX.is_hex(true_value))
								{
									var hex		= true_value;
									var rgb		= hex_to_rgb(hex);

									data['variables'][j]['hex']		= hex;
									data['variables'][j]['rgb']		= rgb;
								}
								else if(true_value.substring(0, 3) == 'rgb')
								{
									// trim spaces
									var rgb							= true_value.replace(/\s/g, '');
										rgb							= rgb.match(REGEX.RGB);

									if(rgb)
									{
										rgb		= {
											r	: Number(rgb[1]),
											g	: Number(rgb[2]),
											b	: Number(rgb[3])
										};

										var hex	= rgb_to_hex(rgb.r, rgb.g, rgb.b);

										data['variables'][j]['hex']	= hex;
										data['variables'][j]['rgb']	= rgb;
									}
								}
							}
						}
					}
				}
			}
			catch(e)
			{
				console.log(e)
			}
								/*

								if(!result[key])
								{
									result[key]		= data[key];
								}
								else
								{
									// TODO: implement how the current version works
									result[key]		= Object.assign(result[key], data[key]);
								}
								*/

								break;

							case 'mixins':
							case 'placeholders':
							case 'functions':

								if(!result[key])
								{
									result[key]		= data[key];
								}
								else
								{
									result[key]		= Object.assign(result[key], data[key]);
								}

								break;

							default :

								if(!result[key])
								{
									result[key]		= data[key];
								}
								else
								{
									result[key]		= result[key].concat(data[key]);
								}

								break;
						}
					}
				}

				delete result.imports;

				resolve(result);
			});
	});
}

// -----------------------------------------------------------------------------
// Exports
// -----------------------------------------------------------------------------

// instance for the parser
let parser						= null;

/**
 * [exports description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
module.exports = function(options)
{
	if(!parser)
	{
		// create a new instance of the parser
		parser						= new Parser(options);

		// update functions
		module.exports.refresh		= parser.refresh.bind(parser);
		module.exports.refreshfile	= parser.refreshfile.bind(parser);
	}

	return parser.compile();
}

module.exports.refresh			= function() {}
module.exports.refreshfile		= function() {}

// -----------------------------------------------------------------------------
// Functions to sort
// -----------------------------------------------------------------------------













var doc_formatters			= {

	all			: function(value)
	{
		if(value === 'true')
		{
			return true;
		}

		if(value === 'false')
		{
			return false;
		}

		return value;
	},

	tags		: function(value)
	{
		var tags				= value.split(',');
			tags				= tags.map(function(item, index){return item.trim().toLowerCase();});
			tags				= tags.sort();

		return tags;
	}
};











Parser.prototype.parse_doc_comment = function(comment)
{
	let self					= this;

	comment						= comment.trim();

	// TODO
	var lines					= comment.split("\n");

	for(var i = 0; i < lines.length; i++)
	{
		lines[i]				= lines[i].trim();
	}

	comment						= lines.join("\n");

	// remove tabs as they're not really necessary
	comment						= comment.replace(/(\t)/g, ' ');

	// remove double spaces
	comment						= comment.replace(/(  )/g, ' ');

	// add an additional @ to the beginning of lines already starting with an @ to produce
	// @@group
	comment						= comment.replace(/\n@/g, '\n@@');

	// now split new lines starting with an @ into an array
	comment						= comment.split(/\n@/);

	var data					= {};

	for(var i = 0; i < comment.length; i++)
	{
		var line				= comment[i].trim();
		var parts				= line.match(/^@([a-z_-]+)(.*)/);

		// if there is no match... assume its the title
		if(!parts)
		{
			data['title']		= line;
		}
		else
		{
			try
			{
				var name			= parts[1],
					value			= parts[2];

				if(value)
				{
					value			= value.trim();
				}

				switch(name)
				{
					case 'desc'	:

						value		= line.replace('@'+name, '');
						value		= value.trim();
						value		= value.replace(/\n/g, '\n\n');

						data[name]	= marked(value);

						break;

					case 'link' :

						var o		= value.match(/((https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([^\s]+))( (.*))?/)

						if(o)
						{
							if(!data[name])
							{
								data[name]			= [];
							}

							data[name].push(
								{
									'url'			: o[1],
									'caption'		: o[7]
								}
							);
						}

						break;

					case 'modifier' :

						var o		= value.split(':');

						var classes	= o.shift();
							classes	= classes.trim();

						var desc	= o.join(':');
							desc	= desc.trim();

						if(!data['modifiers'])
						{
							data['modifiers']		= [];
						}

						data['modifiers'].push(
							{
								'class'		: classes,
								'desc'		: desc
							}
						);

						break;

					case 'param' :

						var o		= value.match(/\{([^}]+)\} (\$[^\s]+)([\s]+)?(\[([^\]]+)\])?([\s]+)?(- (.*))?/);

						if(o)
						{
							if(!data[name])
							{
								data[name]			= [];
							}

							data[name].push(
								{
									'type'			: o[1],
									'name'			: o[2],
									'default'		: o[5] ? o[5].trim() : '-',
									'description'	: o[8] ? o[8].trim() : '',
								}
							);
						}

						break;

					case 'see' :

						/// @see other-item
						/// @see {mixin} other-item
						/// @see $other-item
						/// @see %other-item

						// TODO
						if(!data[name])
						{
							data[name]			= [];
						}

						break;

					case 'require' :

						/// @require item
						/// @require {type} item
						/// @require {type} item - description
						/// @require {type} item description
						/// @require {type} item <link>
						/// @require {type} item description <link>

						if(!data[name])
						{
							data[name]			= [];
						}

						// TODO

						break;

					case 'return' :

						var o			= value.match(/\{([^}]+)\}(.*)?/);

						if(o)
						{
							if(!data[name])
							{
								data[name]			= [];
							}

							data[name]	=
							{
								type		: o[1],
								description	: o[2] ? o[2].trim() : ''
							};
						}

						break;

					case 'since' :

						var o			= value.match(/([0-9.]+)(.*)?/);

						if(o)
						{
							if(!data[name])
							{
								data[name]			= [];
							}

							data[name].push({
								version		: o[1],
								description	: o[2] ? o[2].trim() : ''
							});
						}

						break;

					case 'todo' :
					case 'throw' :
					case 'author' :
					case 'example' :

						if(!data[name])
						{
							data[name]			= [];
						}

						data[name].push(value);
						break;

					case 'group' :

						data[name]			= value;
						data['_group']		= value.toLowerCase();

						// TODO: this better
						data['_path']		= '/styleguide/' + value.toLowerCase() + '/';

						break

					case 'access' :
					case 'alias' :
					case 'content' :
					case 'deprecated' :
					case 'name' :
					case 'output' :
					case 'type' :
					default :

						value		= doc_formatters['all'](value);

						if(doc_formatters[name])
						{
							value		= doc_formatters[name](value);
						}



						if(self.formatters[name])
						{
							value		= self.formatters[name](value);
						}



						if(data[name])
						{
							data[name]	= !Array.isArray(data[name]) ? [data[name]] : data[name]
							data[name].push(value);
						}
						else
						{
							data[name]	= value;
						}





				}
			}
			catch(e)
			{
				//console.log(line);
				//console.log(e);
				//console.log('----------------');
				//console.log(parts);
			}
		}
	}

	return data;
}






Parser.prototype.parse_style_comment = function(comment)
{
	// trim down the comment
	comment						= comment.trim();

	// split the comment into 2 parts, dats and markup
	var parts					= comment.split('---');

	// document the data by callingn the default documentation function
	var data					= this.parse_doc_comment(parts[0]);

	if(parts.length === 2)
	{
		data['_markup']			= parts[1].trim();
	}

	// return the data
	return data;
}






function locate_block(string, start, search)
{
	var start_pos				= string.indexOf(search, start);

	// if there is no occurance of the starting string, bail out
	if(start_pos === -1)
	{
		return false;
	}

	// to match in and out of braces
	var brace_count		= 0;
	var characters		= '';

	for(var position = start_pos; position < string.length; position++)
	{
		// get the character in the mixin
		var character	= string[position];

		if(character === '{')
		{
			brace_count++;
		}

		characters		 += character;

		if(character === '}')
		{
			brace_count--;

			if(brace_count === 0)
			{
				break;
			}
		}
	}

	return {
		string		: characters,
		position	: position
	};
}




function cut_hex(hex)
{
	// if the first character is a #, strip it
	if(REGEX.is_hex(hex))
	{
		return hex.substring(1);
	}

	// retutn the hex value
	return hex;
}

function hex_to_hex(hex)
{
	hex				= cut_hex(hex);

	// if this is a 3 digit hex, convert it to a 6 digiter...
	if(hex.length === 3)
	{
		hex			= hex.charAt(0) + hex.charAt(0) + hex.charAt(1) + hex.charAt(1) + hex.charAt(2) + hex.charAt(2);
	}

	// return the hex value
	return '#' + hex;
}

function hex_to_rgb(hex)
{
	// make sure that the value is a 6 figure hex value
	var hex			= hex_to_hex(hex);

	// remove the beginning #
		hex			= cut_hex(hex);

	// return the rgb value
	return {
		r			: parseInt(hex.substring(0, 2), 16),
		g			: parseInt(hex.substring(2, 4), 16),
		b			: parseInt(hex.substring(4, 6), 16)
	};
}

function rgb_to_hex(r, g, b)
{
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function get_value_tree(name, variables, arr)
{
	// if the array is not defined, set it to a blank array
	if(!arr)
	{
		arr						= [];
	}

	//  make sure that the variable is set
	if(variables[name])
	{

		// get the value of the variable
		var value				= variables[name]['_value'];

		// add the value into the array
		arr.push(value);

		// if the value is a reference to another value, call this function again
		if(value.charAt(0) === '$')
		{
			arr					= get_value_tree(value, variables, arr);
		}

	}

	// return the array tree
	return arr;
}


var REGEX = {
		USERNAME				: /^[a-z0-9_-]{3,16}$/i,
		PASSWORD				: /^[a-z0-9_-]{6,18}$/i,
		HEX						: /^#?([a-f0-9]{6}|[a-f0-9]{3})$/i,
		SLUG					: /^[a-z0-9-]+$/i,
		EMAIL					: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
		URL						: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i,
		IP						: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
		RGB						: /rgb\(([0-9\s]+),([0-9\s]+),([0-9\s]+)/,
		FILE_EXTENSION			: /\.[^.$]+$/,
		DIRECTORY				: /[\/\\]\./,
		/**
		 * [is_hex description]
		 * @param  {[type]}  str [description]
		 * @return {Boolean}	 [description]
		 */
		is_hex : function(str)
		{
			return this.HEX.test(str);
		}
};
