
// -----------------------------------------------------------------------------
// Vars/Constants
// -----------------------------------------------------------------------------

const glob						= require('glob')
	, handlebars				= require('handlebars')
	, fs						= require('graceful-fs')
	, express					= require('express')
	, builder					= require('./builder')
	, bodyparser				= require('body-parser')
	, path						= require('path')
	, browserSync				= require('browser-sync');

const Promise					= global.Promise || require('promise');

let config						= {
	port						: 5000,
	modules						: true,
	reports						: false,
	styleguide					: true,
	styles						: false,
	scripts						: false,
	brand						: true,
	templates					: true,
	get_data					: function(config, req, res)
	{
		return {};
	}
};

module.exports.setup = function(options)
{
	config						= Object.assign({}, config, options);

	// layouts
	config.layoutsDir			= !Array.isArray(config.layoutsDir) ? [config.layoutsDir] : config.layoutsDir;

	config.layoutsDir.unshift(__dirname + '/masters/');


	// partials
	config.partialsDir			= !Array.isArray(config.partialsDir) ? [config.partialsDir] : config.partialsDir;

	config.partialsDir.unshift(__dirname + '/partials/');

	const app					= express()
		, engine				= builder.create(
									{
										pagesDir		: config.pagesDir,
										layoutsDir		: config.layoutsDir,
										partialsDir		: config.partialsDir,
										helpers			: config.helpers,
										debug			: false
									}
								);

	app.engine('html', function(filepath, options, callback)
	{
		// refresh all the data
		engine.refresh();

		fs.readFile(filepath, function (err, content)
		{
			if(err)
			{
				return callback(err, 'nuts');
			}

			engine.render(content.toString(), options)
				.then(function(html)
				{
					return callback(null, html);
				});
		});
	});

	let pages					= !Array.isArray(config.pagesDir) ? [config.pagesDir] : config.pagesDir;

	for(let i = 0; i < pages.length; i++)
	{
		pages[i]				= path.normalize(path.format(
									{
										dir		: process.cwd(),
										base	: pages[i]
									}
								));

		pages[i]				= pages[i].replace(/\//g, '\\');
		pages[i]				= pages[i].split('\*')[0];
		pages[i]				= pages[i].replace(/\\$/, '');
	}

	pages.unshift(__dirname + '/core-pages');

	// specify the views directory
	app.set('views', pages);

	// register the template engine
	app.set('view engine', 'html');

	// add assets
	app.use('/assets', express.static(process.cwd() + '/build/assets'));

	// src
	app.use('/src', express.static(process.cwd() + '/src'));

	//
	app.use('/assets-framework', express.static(__dirname + '/assets'));

	//
	app.use('/placeholder', express.static(__dirname + '/placeholder'));

	// for parsing application/json
	app.use(bodyparser.json());

	// for parsing application/x-www-form-urlencoded
	app.use(bodyparser.urlencoded({ extended: true }));

	function get_config(req, res)
	{
		// TODO: sort this out
		let data				= config.get_data(config, req, res);
		let settings			= Object.assign({}, config);

		data.settings			= settings;

		data.guid				= "TODO-guid";

		data.request			= {
			url					: ""
		};


		if(req)
		{
			data.request.url	= req.path;
			data['path']		= req.originalUrl;
		}

		return data;
	}

	app.all('/', function(req, res)
	{
		res.render('engine-index', Object.assign({}, get_config(req, res), {}) );
	});

	if(config.modules)
	{
		app.all('/modules', function(req, res)
		{
			res.render('engine-modules', Object.assign({}, get_config(req, res), {}) );
		});

		app.all('/module', function(req, res)
		{
			res.render('engine-module', Object.assign({}, get_config(req, res), {}) );
		});
	}

	if(config.reports)
	{
		app.all('/reports', function(req, res)
		{
			res.render('engine-reports', Object.assign({}, get_config(req, res), {}) );
		});
	}

	if(config.styleguide)
	{
		app.all(/\/styleguide(\/)?(.*)?/, function(req, res)
		{
			let data			= Object.assign({}, get_config(req, res), {});

			if(req.query['tag'])
			{
				data._taglist		= [];
				data._tag			= req.query['tag'];

				if(data.styles.tags[req.query['tag']])
				{
					data._taglist	= data.styles.tags[req.query['tag']];
				}
			}

			res.render('engine-styleguide', data);
		});
	}

	if(config.styles)
	{
		app.all('/styles', function(req, res)
		{
			res.render('engine-styles', Object.assign({}, get_config(req, res), {}) );
		});
	}

	if(config.scripts)
	{
		app.all('/scripts', function(req, res)
		{
			res.render('engine-scripts', Object.assign({}, get_config(req, res), {}) );
		});
	}

	if(config.brand)
	{
		app.all('/brand', function(req, res)
		{
			res.render('engine-brand', Object.assign({}, get_config(req, res), {}) );
		});
	}

	if(config.templates)
	{
		app.all(/\/templates(\/)?(.*)?/, function(req, res)
		{
			let page					= req.originalUrl;

			// remove any reference to templates and any file extension, and trim any trailing slashes
			page						= page.replace('/templates', '').replace('.html', '').replace(/^\//, '').replace(/\/$/, '');

			// if the page is blank, then this is the templates hub
			if(page === '')
			{
				res.render('engine-templates', Object.assign({}, get_config(req, res), {}) );
			}
			else
			{
				// TODO: loop through the pages array

				// loop through all the pages
				fs.access(pages[1] + `/${page}.html`, fs.constants.R_OK | fs.constants.W_OK, (err) => {

					// if the file does not exist
					if(err)
					{
						// skip to the next rule - which should be the 404
						req.next();
						return;
					}

					// render the page as expected
					res.render(page, Object.assign({}, get_config(req, res), {}) );
				});
			}
		});
	}

	app.use(function(req, res, next)
	{
		res.render('404', Object.assign({}, get_config(req, res), {
			title				: "We could not find what you where looking for"
		}) );
	});

	app.listen(config.port);

	browserSync.init(
	{
		proxy					: "http://localhost:" + config.port,
		port					: config.port + 1000,
		notify					: false,
		reloadDebounce			: 100,
		logPrefix				: "Template Hub",
		ui						: false
	});
}
